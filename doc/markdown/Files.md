[Back to the Home Page](../../README.md)

# All files in this project

1. Documentation files: `README.md`, `doc/`
2. Maven POM: `pom.xml`
3. Scenario in YAML: `AppleHomePage.yaml`
4. Properties filtered with your environment variables: `test.properties`
5. Java Code: `AppleHomePageTest.java`

Full directory:

````
guixer-getting-started/
    README.md
    doc/
    pom.xml
    src/test/
        resources/
            AppleHomePage.yaml
            test.properties
        java/io/guixer/getting_started/
            AppleHomePageTest.java
````

## 1. Documentation files

* `README.md` — [link](../../README.md)
* `doc/`

Those are simply the files for this documentation.

## 2. Maven POM

* `pom.xml` — [link](../../pom.xml)

This file links to some tools used by this getting started project:

* guixer-tools, to run the tests
* guixer-maven-plugin, to package the screenshots and send them to Guixer

Those dependencies are open source and present
in Maven Central.

The POM file also holds some environment variables
you must set in order to run the tests:

````
<guixer.username>xxx</guixer.username>
<guixer.password>xxx</guixer.password>
<seleniumServer.url>http://xxx:4444/wd/hub</seleniumServer.url>
````

## 3. Scenario in YAML

* `AppleHomePage.yaml` — [link](../../src/test/resources/AppleHomePage.yaml)

This file describes the scenario that will be run
during the test. It starts with a name:

````
name: Apple Home Page
````

After the test are run,
uou will see this name in Guixer and it will ease
the browsing through your test results.

````
steps:
````

This keyword introduces the steps that will be run
during the tests.

**First step:**

````
 - intent: We go to Apple's website
   get: https://apple.com/
   takeScreenshot: true
````

The `intent` keyword is important, since it will
help you navigate through the screenshots in Guixer.

“`get`” simply tells Selenium to go to a given URL.

“`takeScreenshot: true`” is necessary, because
some steps in tests may not take screenshots. For
example, think to a step dedicated to check
several things in the current screen, without
having to take a new screenshot.

**Second step:**

````
 - intent: We click on the "iPhone" link
   click: xpath://a[@aria-label = 'iPhone']
   takeScreenshot: true
````

“`click`” tells Selenium to click on an element in the page. You can use XPath to locate the element.


**Third step:**

````
 - intent: We click on the "iPhone SE" icon
   click: css:li.chapternav-item-iphone-se a
   sleep: 3
   takeScreenshot: true
````

Here, `click` uses a CSS selector to locate the element.

“`sleep`” makes the runner wait for a given amount
of seconds, because modern web apps often load
page fragments asynchronously, and you don’t want
to take a screenshot of a partial page.
We could also have used `waitFor`.


## 4. Properties

* `test.properties` — [link](../../src/test/resources/test.properties)

This file contains properties that will be
made available during the test.

This one only contains the following line:

````
seleniumServer.url: ${seleniumServer.url}
````

The property value is filtered with the
`seleniumServer.url` property you set
in the POM file, and then
used when the test is run.

## 5. Java Code

* `AppleHomePage.java` — [link](../../src/test/java/io/guixer/getting_started/AppleHomePageTest.java)

This holds the Java code that will actually be
run, relying on the
guixer-tools dependency.

````
package io.guixer.getting_started;

import org.junit.jupiter.api.Test;

import io.guixer.tools.GuixerTest;
import io.guixer.tools.Scenario;

public class AppleHomePageTest extends GuixerTest {

	@Test
	@Scenario("AppleHomePage.yaml")
	public void testHomePage() throws Exception {

		run();
	}
}
````

The code is very simple: You only annotate
the test method with
`@Scenario` and give the name of the YAML file
containing the Scenario.

Note: If you omit the name of the YAML file,
or even the `@Scenario` annotation, guixer-tools
will infer the name of the YAML file from the
name of the Java test class.

Then you must call the `run()` method, responsible
to perform the test, instantiating
a `WebDriver` to connect to Selenium + Firefox,
and going through all steps declared in the
scenaro YAML file.

