[Back to the Home Page](../../README.md)

# A diagram of what just happened

![What just happened](../diagrams/what_just_happened.png)

1. You ran the Maven project;
2. It connected to Selenium and launched Firefox;
3. It ran the tests: It connected to https://apple.com/ and got some screenshots;
4. The screenshots were saved locally in the `target` directory managed by Maven;
5. The screenshots and the corresponding log file(s) were put in a ZIP file;
6. The ZIP file was sent to Guixer;
7. Now you can browse in Guixer through the screenshots you took while running the tests.

