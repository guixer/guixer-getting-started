# guixer-getting-started

A sample project that uses Guixer with a minimal YAML scenario.

We connect to [https://apple.com/](https://apple.com/) and get
a few screenshots.

This project is intended to help new users.

## How to proceed

Just clone this project:

````
$ git clone https://gitlab.com/guixer/guixer-getting-started.git
````

Then go through the following instructions.


## 1. Environment

You need:

* Java
* Maven
* An environment where to run Selenium
* Some credentials to connect to Guixer

If you have Docker installed, here is a sample
command to run Selenium:

````
$ docker run -d -p 4444:4444 selenium/standalone-firefox
````

You can run Selenium on another machine than
your workstation. Selenium doesn’t need
neither Maven nor Java.

For instance:

* Run Selenium within Docker on a Linux Machine
* Run guixer-getting-started on a Windows Machine

## 2. Preparation Checks

Once everything is installed,
here are some sample checks that were performed
on a platform where the project runs fine.

Feel free to adapt.

### 2.1. Java

````
$ java -version
java 17.0.6 2023-01-17 LTS
Java(TM) SE Runtime Environment (build 17.0.6+9-LTS-190)
Java HotSpot(TM) 64-Bit Server VM (build 17.0.6+9-LTS-190, mixed mode, sharing)
````

### 2.2. Maven

````
$ mvn --version
Apache Maven 3.9.1 (2e178502fcdbffc201671fb2537d0cb4b4cc58f8)
Maven home: /opt/homebrew/Cellar/maven/3.9.1/libexec
Java version: 19.0.2, vendor: Homebrew, runtime: /opt/homebrew/Cellar/openjdk/19.0.2/libexec/openjdk.jdk/Contents/Home
Default locale: en_FR, platform encoding: UTF-8
OS name: "mac os x", version: "13.3.1", arch: "aarch64", family: "mac"

````


### 2.3. Selenium

````
$ curl http://localhost:4444/status
   ...
        "version": "4.8.1 (revision 8ebccac989)",
          ...
            "stereotype": {
              "browserName": "firefox",
              "browserVersion": "110.0",
              "platformName": "LINUX",
              "se:noVncPort": 7900,
              "se:vncEnabled": true
            }
````

Don’t panic if 
`http://localhost:4444/wd/hub` (that
is, the value you will put in the POM file)
doesn’t respond. It is meant
to be used by the automated
tests, and not from the command line.

### 2.4. Guixer Credentials

Go to [https://guixer.io/](https://guixer.io/) with
a web browser, and check your credentials.

## 3. Configuration

In the **pom.xml** file of this project,
you will find the following placeholders:

````
<guixer.username>xxx</guixer.username>
<guixer.password>xxx/guixer.password>
````

You need to fill those placeholders with your credentials.

Also this:

````
<seleniumServer.url>http://192.168.0.15:4444/wd/hub</seleniumServer.url>
````

You need to provide your own local IP Address
where Selenium is running.

## 4. What is going to happen now



1. You will run the Maven project;
2. It will connect to Selenium and launch Firefox;
3. It will run the tests: It will connect to https://apple.com/ and get some screenshots;
4. The screenshots will be saved locally in the `target` directory managed by Maven;
5. The screenshots and the corresponding log file(s) will be put in a ZIP file;
6. The ZIP file will be sent to Guixer;
7. You will be able to browse in Guixer through the screenshots you took while running the tests.

## 5. Run the Maven project

Type:

````
$ mvn install
````

That is the run part.


## 6. Browse Guixer

Your screenshots are there.

That is the browse part.

## 7. Want more info?

[A diagram of what just happened there](doc/markdown/Diagrams.md)

[All files in this getting-started project, explained](doc/markdown/Files.md)


